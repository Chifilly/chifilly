module.exports = {
	ignorePatterns: [
		'src/environments/**/*.prod.ts',
		'src/polyfills*.ts'
	],
	overrides: [
		{
			// TYPESCRIPT
			files: ['*.ts'],
			parser: '@typescript-eslint/parser',
			parserOptions: {
				ecmaVersion: 2020,
				sourceType: 'module',
				project: './src/tsconfig.app.json'
			},
			env: {
				browser: true,
				es6: true,
				node: true
			},
			plugins: [
				'@typescript-eslint',
				'@typescript-eslint/tslint',
				'@angular-eslint'
			],
			rules: {
				'@typescript-eslint/naming-convention': [
					'error',
					{
						'selector': 'default',
						'format': ['camelCase'],
						'trailingUnderscore': 'forbid'
					},
					{
						'selector': ['variable', 'function'],
						'format': ['camelCase'],
						'leadingUnderscore': 'allow'
					},
					{
						'selector': ['class'],
						'format': ['PascalCase'],
					},
					{
						'selector': 'memberLike',
						'modifiers': ['private'],
						'format': ['camelCase'],
						'leadingUnderscore': 'require'
					},
					{
						'selector': 'variable',
						'types': ['boolean'],
						'format': ['PascalCase'],
						'prefix': ['is', 'should', 'has', 'can', 'did', 'will', 'was']
					},
					{
						'selector': 'typeLike',
						'format': ['PascalCase'],
						'prefix': ['T'],
					},
					{
						'selector': 'interface',
						'format': ['PascalCase'],
						'custom': {
							'regex': '^I[A-Z][A-Za-z]*',
							'match': true
						}
					},
					{
						'selector': 'enumMember',
						'format': ['UPPER_CASE']
					}
				],
				'@typescript-eslint/consistent-type-definitions': 'error',
				'@typescript-eslint/explicit-member-accessibility': [
					'off',
					{
						'accessibility': 'explicit'
					}
				],
				'@typescript-eslint/indent': [
					'error',
					'tab',
					{
						'ArrayExpression': 'first',
						'ObjectExpression': 'first',
						'FunctionDeclaration': {
							'parameters': 'first'
						},
						'FunctionExpression': {
							'parameters': 'first'
						},
						'SwitchCase': 1
					}
				],
				'@typescript-eslint/member-delimiter-style': [
					'error',
					{
						'multiline': {
							'delimiter': 'semi',
							'requireLast': true
						},
						'singleline': {
							'delimiter': 'semi',
							'requireLast': false
						}
					}
				],
				'@typescript-eslint/member-ordering': 'off',
				'@typescript-eslint/no-empty-function': 'off',
				'@typescript-eslint/no-empty-interface': 'error',
				'@typescript-eslint/no-inferrable-types': 'off',
				'@typescript-eslint/no-misused-new': 'error',
				'@typescript-eslint/no-non-null-assertion': 'error',
				'@typescript-eslint/prefer-function-type': 'error',
				'@typescript-eslint/quotes': [
					'error',
					'single'
				],
				'@typescript-eslint/semi': [
					'error',
					'always'
				],
				'@typescript-eslint/type-annotation-spacing': 'error',
				'@typescript-eslint/unified-signatures': 'error',
				'arrow-body-style': [
					'error',
					'always'
				],
				'brace-style': [
					'warn',
					'allman',
					{
						'allowSingleLine': false
					}
				],
				'camelcase': 'off',
				'constructor-super': 'error',
				'curly': 'error',
				'dot-notation': 'off',
				'eol-last': 'error',
				'eqeqeq': [
					'error',
					'smart'
				],
				'guard-for-in': 'error',
				'id-blacklist': 'off',
				'id-match': 'off',
				'indent': 'off',
				'max-len': 'off',
				'no-bitwise': 'off',
				'no-caller': 'error',
				'no-console': [
					'error',
					{
						'allow': [
							'log',
							'warn',
							'dir',
							'timeLog',
							'assert',
							'clear',
							'count',
							'countReset',
							'group',
							'groupEnd',
							'table',
							'dirxml',
							'error',
							'groupCollapsed',
							'Console',
							'profile',
							'profileEnd',
							'timeStamp',
							'context'
						]
					}
				],
				'no-debugger': 'error',
				'no-empty': 'off',
				'no-eval': 'error',
				'no-fallthrough': 'error',
				'no-new-wrappers': 'error',
				'no-restricted-imports': [
					'error',
					'rxjs',
					'rxjs/Rx'
				],
				'no-shadow': 'off',
				'no-throw-literal': 'error',
				'no-trailing-spaces': 'error',
				'no-undef-init': 'error',
				'no-underscore-dangle': 'off',
				'no-unused-expressions': 'error',
				'no-unused-labels': 'error',
				'no-var': 'error',
				'prefer-const': 'error',
				'radix': 'error',
				'spaced-comment': 'error',
				'@typescript-eslint/tslint/config': [
					'error',
					{
						'rulesDirectory': ['codelyzer'],
						'rules': {
							'component-class-suffix': true,
							'component-selector': [
								true,
								'element',
								'app',
								'kebab-case'
							],
							'directive-selector': [
								true,
								'attribute',
								'app',
								'camelCase'
							],
							'no-host-metadata-property': true,
							'no-input-rename': true,
							'no-inputs-metadata-property': true,
							'no-output-on-prefix': true,
							'no-output-rename': true,
							'no-outputs-metadata-property': true,
							'use-lifecycle-interface': true,
							'use-pipe-transform-interface': true,
							'whitespace': [
								true,
								'check-decl',
								'check-operator',
								'check-separator',
								'check-type'
							]
						}
					}
				]
			}
		},
		{
			// TESTS
			files: ['*.spec.ts'],
			parser: '@typescript-eslint/parser',
			parserOptions: {
				ecmaVersion: 2020,
				sourceType: 'module',
				project: './src/tsconfig.spec.json'
			},
		},
		{
			// HTML
			files: ['*.html'],
			parser: '@angular-eslint/template-parser',
			plugins: ['@angular-eslint/template'],
			rules: {

			},
		}
	],
};
